This node is specifically designed to work with the home control software at http://tech.scargill.net 
and both outputs info to our ESP8266 boards to give them time, date and dusk dawn info - and also 
outputs logging info to MSQL and SQLITE databases as well as for a flat output file. 

Output is generated every 12 hours to all units (toesp) and to any unit requesting 
info (xxx/toesp) by sending a json payloda including it's ID (xxx.id)

The information includes the time, dusk and dawn info for a given long/lat.
Search on the site for "home control 2015"